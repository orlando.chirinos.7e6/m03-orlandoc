/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/19
* TITLE: Calcula la media
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */

fun main (args: Array<String>){
    // for (i in args.indices)
    // Asigna a i un valor numérico dependiendo de la posición.
    //for (i in args.iterator())
    //Asigna a i el valor del elemento del array.
    //for (i in 0 until args.size)
    // Asigna a i un valor numérico dependiendo de la posición. (usa el indices)
    // args.forEach { it }
    //"it" = array/args de esa vuelta. iterator.
    var resultado = 0.0
    for (i in args.iterator()){
        resultado += i.toInt()
    }
    print(resultado/args.size)
}