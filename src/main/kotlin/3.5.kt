/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/13
* TITLE: Tabla de multiplicar
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */
import java.util.*
fun main (){
    val scan = Scanner(System.`in`)
    print("Introduzca número a encontrar tabla de multiplicar: ")
    var intnumber = scan.nextInt()

    for (i in 1 ..10) {
        println ("$intnumber \bx$i=${intnumber*i}")
    }

}