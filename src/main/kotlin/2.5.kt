import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: En rango
*/
fun main() {

    val scanner = Scanner(System.`in`)
    print("Inserte primer rango: ")
    var firstnum = scanner.nextInt()
    var secondnum = scanner.nextInt()
    print("Inserte segundo rango: ")
    var thirdnum = scanner.nextInt()
    var fourthnum = scanner.nextInt()
    println("Inserte número a decidir a qué rango pertenece")
    var fifthnum = scanner.nextInt()
    if (fifthnum >= firstnum && fifthnum <= secondnum || fifthnum >= thirdnum && fifthnum <= fourthnum) {
        print(true)
    }
        else {
        print(false)
    }
}
