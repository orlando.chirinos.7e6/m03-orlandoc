/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Pupitres
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce primera cantidad de alumnos: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputvalue = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduce segunda cantidad de alumnos: ")
    val inputvalue2 = escaneo.nextInt()
    val result1 = inputvalue + inputvalue2
    print("Introduce tercera cantidad de alumnos: ")
    val inputvalue3 = escaneo.nextInt()
    println("")
    val totalpupitres = inputvalue + inputvalue2 + inputvalue3
    val total = (totalpupitres / 2) + 1
    print("La cantidad de pupitres necesarios teniendo en cuenta los alumnos es: ")
    print(total)

}