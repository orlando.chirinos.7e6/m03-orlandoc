/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Calcula el área
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce el amplio (en metros): ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputvalue = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduce el largo (en metros): ")
    val inputvalue2 = escaneo.nextInt()
    val sumado = inputvalue * inputvalue2
    print("El área es: ")
    print(sumado)
    print(" m2")
}