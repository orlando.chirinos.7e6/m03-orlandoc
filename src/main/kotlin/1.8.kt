/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Dobla el decimal
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca número decimal: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    var read = escaneo.nextDouble()
    //variable escaneo usa el "poder" de la librería "NEXT.INT()"
    var next = read * 2
    print("Después va el: ")
    print(next)

}