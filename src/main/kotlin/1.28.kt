/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/23
* TITLE: Uno es 10
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca primer número: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val firstNum = escaneo.nextInt()
    print("Introduzca segundo número: ")
    val secondNum = escaneo.nextInt()
    print("Introduzca tercer número: ")
    val thirdNum = escaneo.nextInt()
    print("Introduzca cuarto número: ")
    val fourthNum = escaneo.nextInt()
    val oneIsTen = (firstNum == 10) || (secondNum == 10) || (thirdNum == 10) || (fourthNum == 10)
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("¿Es uno de los números introducidos igual a \"10\"?: $oneIsTen")
}