import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/06
* TITLE: Salario
*/
fun main(){

    val scanner = Scanner(System.`in`)
    print("Inserte cantidad de horas trabajadas: ")
    var hours = scanner.nextInt()
    var extra = 0
    var money = 0

    if (hours <= 40){
        var money = hours * 40
        print("El importe a recibir es: $money \b EUR")
    }
    else {
        extra = (hours %40)*20
        money = (hours * 40)+extra
        print("El importe a recibir es: $money \b EUR")
    }
}