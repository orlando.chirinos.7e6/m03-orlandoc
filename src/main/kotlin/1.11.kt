/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Calculadora de volumen de aire
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce el largo de la habitación (metros y decimal): ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val length = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduce el amplio de la habitación (metros y decimal): ")
    val wide = escaneo.nextDouble()
    print("Introduce la altura de la habitación(metros y decimal): ")
    val height = escaneo.nextDouble()
    var volum = length * wide * height
    println("")
    print("El volúmen de la habitación es: ")
    print(volum)
    print("m3")

}