import java.util.Scanner

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/11/10
* TITLE: Triangulo invertido de *
*/

fun main(){
    val scan = Scanner(System.`in`)
    print("Introduzca número límite a escalera de asteriscos invertidos: ")

    var intnum = scan.nextInt()

    for (i in intnum downTo 1){
        for (X in i until  intnum){
            print(" ")
        }
        for (m in 1 .. i){
            print("*")
        }
        println("")
    }
}