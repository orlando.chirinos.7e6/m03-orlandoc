/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: Pinta X numeros
*/

import java.util.*
fun main(){
    val scan = Scanner(System.`in`)
    print("Inserte número final para contar desde el 1: ")
    val intnumber = scan.nextInt()
    var countnumber = 1
    while (countnumber <= intnumber){
        println(countnumber)
        countnumber++
    }
}
