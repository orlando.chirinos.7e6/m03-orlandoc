/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Divisor de cuenta
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca número de conmensales: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val client = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduzca precio de la cuenta: ")
    val price = escaneo.nextDouble()
    val total = price / client
    print("Cada comensal debe pagar: ")
    print(total)
    print("€")

}