/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: Calcula-dora
*/

import java.util.*

fun main(){
    val scan=Scanner(System.`in`)
    println("Bienvenido a la calculadora. Introduzca ambos números enteros y la operación a realizar.")
    val firstnumber = scan.nextInt()
    val secondnumber = scan.nextInt()
    val operation = scan.next()

    when (operation){
        "+" -> print("${firstnumber + secondnumber}")
        "-" -> print("${firstnumber - secondnumber}")
        "*" -> print("${firstnumber * secondnumber}")
        "/" -> print("${firstnumber / secondnumber}")
        "%" -> print("${firstnumber % secondnumber}")
    }
}