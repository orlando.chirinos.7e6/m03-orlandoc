import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/13
* TITLE: Hackaton
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */
//FOR repite X cantidad de veces (siendo X el número último) lo que se le pida.

fun main(){
    var scan = Scanner(System.`in`)
    var plus = 0

    for (x in 1 .. 100){
        plus += x
    }
    print(plus)
}