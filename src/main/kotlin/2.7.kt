/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: ¿Qué pizza es más grande?
*/
import java.util.*
fun main(args: Array<String>){
    val scan = Scanner(System.`in`)
    print("Introduzca número: ")
    var int = scan.nextInt()
    val ver = int %2
    if (ver == 0){
        print("El número $int es par")
    }
    else {
        print("El número $int es impar")
    }
}