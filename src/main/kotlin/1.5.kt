/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Operación loca
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce primer valor: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputvalue = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduce segundo valor: ")
    val inputvalue2 = escaneo.nextInt()
    val result1 = inputvalue + inputvalue2
    print("Introduce tercer valor: ")
    val inputvalue3 = escaneo.nextInt()
    print("Introduce cuarto valor: ")
    val inputvalue4 = escaneo.nextInt()
    println("")
    val result2 = inputvalue3 % inputvalue4
    val total = result1 * result2
    print("Resultado de la operación: ")
    print(total)

}