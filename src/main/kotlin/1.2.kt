/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Dobla el entero
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    println("Introduce número a duplicar: ")

    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputvalue = escaneo.nextInt()
    val duplicado = inputvalue * 2
    println("Número después de ser duplicado es: ")
    print(duplicado)
}