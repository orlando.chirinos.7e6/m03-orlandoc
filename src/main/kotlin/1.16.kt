/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Transforma el entero
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca entero: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val entero = escaneo.nextInt().toDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("En decimal es: ")
    print(entero)
}