/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/22
* TITLE: Hazme mayúscula
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca caracter: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputValue = escaneo.next().single()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val upperCase = inputValue.uppercase()
    println("Letra en mayúsculas: $upperCase ")
}