/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/13
* TITLE: Es primo?
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */

import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val checkprime = scanner.nextInt()
    var numdiv = 0
    for (i in 1 .. checkprime){
        if (checkprime%i == 0) numdiv++
    }
    if (numdiv == 2) println("Es primo")
    else print("No es primo")
}
