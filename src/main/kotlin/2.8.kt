import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/06
* TITLE: Hazme mayúscula
*/

fun main(){

    val scanner = Scanner(System.`in`)
    print("Inserte hora: ")
    var hour = scanner.nextInt()
    print("Inserte minutos: ")
    var minutes = scanner.nextInt()
    print("Inserte inserte segundos: ")
    var seconds = scanner.nextInt()

    if (++seconds == 60){
        seconds = 0
        minutes += 1
    }
    if (minutes == 60){
        minutes = 0
        hour += 1
    }
    if (hour == 24) hour = 0

    val secondsfinal = if (seconds < 10) {"0$seconds"} else "$seconds"
    val minutesfinal = if (minutes < 10) {"0$minutes"} else "$minutes"
    val hoursfinal = if (hour < 10) {"0$hour"} else "$hour"

    println ("$hoursfinal,:$minutesfinal,:$secondsfinal")

}
