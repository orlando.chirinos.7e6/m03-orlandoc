import java.util.Scanner

fun main() {
    val scan = Scanner(System.`in`)
    print("Introduzca límite de triangulo inverso: ")
    var intnum = scan.nextInt()

    for (i in intnum downTo 0) {
        for (n in intnum downTo i) {
            print(n)
        }
        println("")
    }
}

