/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: Calcula la suma de los N numeros
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */

import java.util.*
fun main() {
    val scan = Scanner(System.`in`)
    print("Inserte número límite: ")
    var intnumber = scan.nextInt()
    var sumnumber = 0
    for (i in 1..intnumber){
        sumnumber = sumnumber + i
    }
    println(sumnumber)
}