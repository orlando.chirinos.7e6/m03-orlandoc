import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: Calcula la letra del DNI
*/

fun main(){
    val scan = Scanner(System.`in`)
    print("Introduzca número de DNI: ")
    val numdni = scan.next()
    when (numdni.toInt() %23) {
        0 -> print("El DNI completo es el siguiente: $numdni\bT")
        1 -> print("El DNI completo es el siguiente: $numdni\bR")
        2 -> print("El DNI completo es el siguiente: $numdni\bW")
        3 -> print("El DNI completo es el siguiente: $numdni\bA")
        4 -> print("El DNI completo es el siguiente: $numdni\bG")
        5 -> print("El DNI completo es el siguiente: $numdni\bM")
        6 -> print("El DNI completo es el siguiente: $numdni\bY")
        7 -> print("El DNI completo es el siguiente: $numdni\bF")
        8 -> print("El DNI completo es el siguiente: $numdni\bP")
        9 -> print("El DNI completo es el siguiente: $numdni\bD")
        10 -> print("El DNI completo es el siguiente: $numdni\bX")
        11 -> print("El DNI completo es el siguiente: $numdni\bB")
        12 -> print("El DNI completo es el siguiente: $numdni\bN")
        13 -> print("El DNI completo es el siguiente: $numdni\bJ")
        14 -> print("El DNI completo es el siguiente: $numdni\bZ")
        15 -> print("El DNI completo es el siguiente: $numdni\bS")
        16 -> print("El DNI completo es el siguiente: $numdni\bQ")
        17 -> print("El DNI completo es el siguiente: $numdni\bV")
        18 -> print("El DNI completo es el siguiente: $numdni\bH")
        19 -> print("El DNI completo es el siguiente: $numdni\bL")
        20 -> print("El DNI completo es el siguiente: $numdni\bC")
        21 -> print("El DNI completo es el siguiente: $numdni\bK")
        22 -> print("El DNI completo es el siguiente: $numdni\bE")
    }
}