/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/22
* TITLE: ¿Somos iguales?
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca primera letra: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputValue1 = escaneo.next().single()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val lowerCase = inputValue1
    print("Introduzca segunda letra: ")
    val inputValue2 = escaneo.next().single()
    print("¿Son ambas letras iguales?: ${inputValue1.toUpperCase() == inputValue2}")

}