import java.util.Scanner

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/24
* TITLE: Triangulo de numeros
*/

fun main(){
    val scan = Scanner(System.`in`)
    print("Introduzca número límite a escalera de números:")
    var intnum = scan.nextInt()

    for (i in 1 .. intnum){
        for (m in 1 .. i){
            print(m)
        }
        println("")
    }
}