import java.util.*

fun main(args: Array<String>){
    var min = args[0].toInt()
    var max = args[0].toInt()
    for (number in args){
        if(number.toInt() > max) max = number.toInt()
        else if (number.toInt() < min) min = number.toInt()
    }
    println("Min: $min")
    println("Max: $max")
}