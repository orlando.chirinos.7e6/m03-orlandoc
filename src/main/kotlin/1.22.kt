/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/22
* TITLE: Quien rie ultimo rie mejor
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca primer nivel de risa (número): ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val laughOne = escaneo.nextInt()
    print("Introduzca segundo nivel de risa (número): ")
    val laughTwo = escaneo.nextInt()
    print("Introduzca tercer nivel de risa (número): ")
    val laughThree = escaneo.nextInt()
    print("Introduzca cuarto nivel de risa (número): ")
    val laughFour = escaneo.nextInt()
    print("Introduzca último nivel de risa (número): ")
    val lastLaugh = escaneo.nextInt()
    val bestLaugh = (lastLaugh > laughOne) && (lastLaugh > laughTwo) && (lastLaugh > laughThree) && (lastLaugh > laughFour)
            //variable escaneo usa el "poder" "NEXT.INT()"
    print("En este caso, el dicho Quien ríe \"Quien ríe último ríe mejor\" es: $bestLaugh")
}