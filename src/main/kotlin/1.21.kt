/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/22
* TITLE: Tres numeros iguales
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca primer número: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val numberOne = escaneo.nextInt()
    print("Introduzca segundo número: ")
    val numberTwo = escaneo.nextInt()
    print("Introduzca tercer número: ")
    val numberThree = escaneo.nextInt()
    val andTest = (numberOne == numberTwo) && (numberOne == numberThree) && (numberTwo == numberThree)
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("La respuesta a la pregunta ¿Son los 3 números iguales? es: $andTest")
}