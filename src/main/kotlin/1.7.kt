/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Número siguiente
*/
import java.util.*
fun main(args: Array<String>) {

    print("Introduzca número entero: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    var read = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    var next = read + 1
    print("Después va el: ")
    print(next)

}