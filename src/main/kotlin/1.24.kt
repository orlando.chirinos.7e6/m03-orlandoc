/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/22
* TITLE: ¿Es un número?
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca número de un caracter: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val inputValue = escaneo.next().single()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val numOrNot = (inputValue >= '0' && inputValue <= '9')
    println("¿Es el caracter introducido un número?: $numOrNot ")
}