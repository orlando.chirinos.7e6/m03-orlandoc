/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Calcula el descuento
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce precio del primer producto: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val read = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduce precio del primer producto pero con su descuento: ")
    val read2 = escaneo.nextDouble()
    val total = ((read2 / read)*100)-100
    print("El descuento aplicado es: ")
    print(total)
    print("%")
}