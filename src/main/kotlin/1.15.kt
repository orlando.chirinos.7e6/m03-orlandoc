/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Divisor de cuenta
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca segundo: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val second = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val count = (second + 1) % 60
    print("El siguiente segundo es: ")
    print(count)
    print("s")

}