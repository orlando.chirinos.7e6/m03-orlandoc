/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: ¿Qué temperatura hace?
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca temperatura original (celsius): ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val temperature = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print("Introduzca aumento de temperatura (celsius): ")
    val increase = escaneo.nextDouble()
    val total = temperature + increase
    print("La temperatura actual es: ")
    print(total)
    print("ºC")

}