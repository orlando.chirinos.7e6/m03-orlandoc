fun main(){
    val array = arrayOf(1,2,3,4,-5,6,-7,8,9)
    var result = 1

    for (i in 0..array.lastIndex){
        if (array [i] < 0) array[i] *= i
        result *= array[i]
    }
    print(result)
}