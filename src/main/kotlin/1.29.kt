/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/29
* TITLE: Ecuaciones de segundo grado
*/
import java.util.*
import kotlin.math.sqrt

//importo libreria de java (ver en un futuro)
fun main() {

    print("Para descubrir el valor de X Introduzca valor a: ")
    val escaneo = Scanner(System.`in`)
    val a = escaneo.nextFloat()
    print("Introduzca valor b: ")
    val b = escaneo.nextFloat()
    val exponente = 2
    print("Introduzca valor c: ")
    val c = escaneo.nextFloat()
    val xPlusNum = (-b + sqrt((Math.pow(b.toDouble(),exponente.toDouble())) - (4 * a * c)))/2*a
    val xMinusNum = (-b - sqrt((Math.pow(b.toDouble(),exponente.toDouble())) - (4 * a * c)))/2*a
    print("El resultado 1 es: $xPlusNum. \nEl resultado 2 es: $xMinusNum.")
}