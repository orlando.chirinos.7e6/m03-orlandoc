/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/13
* TITLE: Imprime el rango (2)
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */


import java.util.*
fun main() {
    val scan = Scanner(System.`in`)
    print("Inserte primer numero de rango: ")
    var firstnum = scan.nextInt()
    print("Inserte segundo número de rango: ")
    var secondnum = scan.nextInt()
    if (firstnum < secondnum) {
        for (i in firstnum..secondnum) {
            print("$i,")
        }
    }
    else{
        for (i in firstnum downTo secondnum){
            print("$i,")
        }
    }
}