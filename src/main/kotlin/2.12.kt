/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/11/11
* TITLE: Calcula-dora
*/
import java.util.Scanner
fun main(args: Array<String>){
    val scan = Scanner(System.`in`)
    println("Introduzca la fecha: ")
    var day = scan.nextInt()
    var month = scan.nextInt()
    var year = scan.nextInt()
    //===Meses y fechas válidas===
    val notnatmonth = arrayListOf(2, 4, 6, 9, 11)
    val validday = (1..31)
    //============================
    if (day in validday && day > 28 && month == 2||
            day > 31 || month > 12||
            day in validday && day > 30 && month in notnatmonth){
            println("La fecha \"$day $month $year\" no es válida.")
    }
    else{
        println("La fecha \"$day $month $year\" es válida.")
    }
}