import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/19
* TITLE: Calcula letra DNI
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */


fun main (args: Array<String>){

    print("Introduce tu número de DNI: ")
    val scanner = Scanner(System.`in`)
    var num = scanner.nextInt()
    var resultado = num % 23

    var dni = arrayOf("T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E")

    println("$num${dni[resultado]}")
}