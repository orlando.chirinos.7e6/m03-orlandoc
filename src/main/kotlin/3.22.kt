/* import java.util.*
import kotlin.text.Typography.times
import kotlin.math.*

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    for (i in 1..2 * n) {
        val space = abs(n:n-i)
        val asterisco = if (2*i - 1 < 2*n) 2 * i - 1
                        else (2*i-1)-(abs(n:n-i)*4)
        repeat(space) {print(" ")}
        repeat(asterisco) {print("*")}
        println()
    }
}
/*
{
    val scanner = Scanner(System.`in`)
    var intnum = scanner.nextInt()

    for (i in 1 .. intnum){
        print(" ")
        for (n in (1*2)-1)
    }

}
*/