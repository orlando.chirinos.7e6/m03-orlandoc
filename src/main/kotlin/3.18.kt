
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/26
* TITLE: Fibonacci
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */

import java.util.*
fun main(){
    val scan = Scanner(System.`in`)
    print("Introduzca límite de Fibonacci: ")
    var limit = scan.nextInt()
    var f1 = 1
    var f2 = 0

    print("0 1 ")

    for (i in 1 until  limit-1){
        var fsum = f1 + f2
        print("$fsum ")
        f2 = f1
        f1 = fsum


    }



}