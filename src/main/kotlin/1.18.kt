/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Es mayor que
*/
import java.util.*
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca primer número: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val first = escaneo.nextInt()
    print("Introduzca segundo número: ")
    val second = escaneo.nextInt()
    //variable escaneo usa el "poder" "NEXT.INT()"
    print(first > second)
}