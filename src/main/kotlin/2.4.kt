import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/06
* TITLE: Tiene edad para trabajar
*/
fun main() {

    val scanner = Scanner(System.`in`)
    print("Inserte su edad: ")
    var edad = scanner.nextInt()
    if (edad > 18) {
        print("Tiene edad para trabajar.")
    } else {
        print("No tiene edad para trabajar.")
    }
}
