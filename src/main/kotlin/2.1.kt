/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/29
* TITLE: Máximo 3 numeros enteros
*/
import java.util.*
fun main(){

    print("Introduzca primer número: ")
    val escaneo = Scanner(System.`in`)
    val a = escaneo.nextInt()
    print("Introduzca segundo número: ")
    val b = escaneo.nextInt()
    print("Introduzca tercer número: ")
    val c = escaneo.nextInt()
    if (a > b && a > c)
        print("El número mayor es: $a.")
    else if ( b > a && b > c ) print("El número mayor es: $b.")
    else if (c > a && c > b) print("El número mayor es $c.")

}