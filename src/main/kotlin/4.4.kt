import java.util.*

/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/21
* TITLE: Está dentro?
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */


fun main (args: Array<String>) {

    print("Introduce numero: ")
    val scanner = Scanner(System.`in`)
    val inputarray = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    val number = scanner.nextInt()

    if (number in inputarray) println("Si está dentro")
    else println("No está dentro")

}