/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/13
* TITLE: Elévalo
*/
/*i se inicializa en FOR, tiene el valor inicial del número después de "IN" */
import java.util.*
fun main (){
    val scan = Scanner(System.`in`)
    print("Introduzca número a elevar: ")
    var intnumber = scan.nextInt()
    print("Introduzca el \"elevado a\": ")
    var elevadoa= scan.nextInt()
    var final = 1L

    for (i in 1 ..   elevadoa){
        final *= intnumber
    }
    print(final)
}