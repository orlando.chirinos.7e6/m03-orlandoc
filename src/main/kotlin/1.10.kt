/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: ¿Cual es la medida de mi pizza?
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduce el diametro de la pizza: ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val diameter = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val ratio = diameter / 2
    val surface = Math.PI * (ratio * ratio)
    print("El área de la pizza es: ")
    print(surface)
    print(" cm2")
}