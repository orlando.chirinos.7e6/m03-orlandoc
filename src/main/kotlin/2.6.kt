/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/10/07
* TITLE: ¿Qué pizza es más grande?
*/

import java.util.*
fun main (){

    val scan = Scanner(System.`in`)
    print ("Introduzca diámetro de la pizza redonda: ")
    var rpizza = scan.nextInt()
        rpizza = rpizza/2
    print ("Introduzca lado 1 de pizza rectangular: ")
    var rec1pizza = scan.nextInt()
    print ("Introduzca lado 2 de pizza rectangular: ")
    var rec2pizza = scan.nextInt()
    val rareapizza = 3.1416 * (rpizza * rpizza)
    val recareapizza = rec1pizza * rec2pizza
    if (rareapizza > recareapizza){
        print("La pizza redonda es más grande, ya que tiene un área de: $rareapizza \bcm2")
    }
    else {
        print("La pizza rectangular es más grande, ya que tiene un área de: $recareapizza \bcm2")
    }

}